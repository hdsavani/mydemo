<!-- Modal -->
<div class="modal fade" id="create-{{ $moduleTitleS }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Create {{ ucwords($moduleTitleS) }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => $moduleTitleP.'.store','method'=>'POST','autocomplete'=>'off')) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Fullname:</strong>
                                {!! Form::text('fullname', Input::get('fullname'), array('placeholder' => 'Fullname','class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Email:</strong>
                                {!! Form::text('email', Input::get('email'), array('placeholder' => 'Email','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Age:</strong>
                                {!! Form::text('age',null, array('placeholder' => 'Age','class'=>'form-control edit-password','AutoComplete'=>'off')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <strong>Sex:</strong>
                            <div class="form-group">
                                <label>{!! Form::radio('gender', '0', true, array('class' => '')) !!} Male</label>                                
                                <label>{!! Form::radio('gender', '1', false, array('class' => '')) !!} Female</label>                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Latitude:</strong>
                                {!! Form::text('latitude', Input::get('latitude'), array('placeholder' => 'Latitude','class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Longitude:</strong>
                                {!! Form::text('longitude', Input::get('longitude'), array('placeholder' => 'longitude','class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Profile Picture:</strong>
                                {!! Form::file('image', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="pull-right">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary create-crud create-crud">Submit</button>
                        
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>