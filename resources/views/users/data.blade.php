@if(!empty($data) && $data->count())
    @foreach($data as $key => $value)
        <tr>
            <td>{{ ++$i }}</td>
            <td>
                @if(!is_null($value->main_image))
                <img src="{{ $value->main_image }}" style="width:40px;height:40px">
                @else
                <img src="/newTheme/img/ld.png" style="width:40px;height:40px">
                @endif
            </td>
            <td>{{ $value->fullname }}</td>
            <td>{{ $value->email }}</td>
            <td>
                @if($value->ban == 0)
                <label class="badge badge-success">No</label>
                @else
                <label class="badge badge-danger">Yes</label>
                @endif
            </td>
            <td>{{ myDateFormat('Y-m-d H:i:s', $value->created_at) }}</td>
            <td>
                @if($value->ban == 1)
                    <a href="{{ route('admin.users.revokeuser',$value->id) }}" class="padding-bt btn btn-sm btn-warning"><i aria-hidden="true" class="fa fa-circle-o"></i> Revoke</a>
                @else
                    <a class="btn btn-success btn-sm ban padding-bt" data-id="{{ $value->id }}" data-action="{{ URL::route('admin.users.ban') }}"><i aria-hidden="true" class="fa fa-ban"></i> Ban</a>
                @endif
                <button class="padding-bt btn btn-sm btn-primary" data-toggle="modal" data-target="#{{ $moduleTitleS }}-{{ $value->id }}"><i aria-hidden="true" class="fa fa-edit"></i> Edit</button>
                @include($moduleTitleP.'.edit')
                <button class="padding-bt btn btn-sm btn-danger remove-crud" data-id="{{ $value->id }}" data-action="{{ route($moduleTitleP.'.destroy',$value->id) }}"><i aria-hidden="true" class="fa fa-trash-o"></i> Delete</button>
                <a href="{{ route('manage.media',['users',$value->id]) }}" class="btn btn-info btn-sm" style="margin-top: 3px;"><i class="fa fa-image"></i> Manage Images</a>
            </td>
        </tr>
    @endforeach
@endif