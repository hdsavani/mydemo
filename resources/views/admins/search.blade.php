<div class="panel panel-default filter-panel filter-panel-custom {{Input::get("filter",false)?'filter-in':'filter-out'}}">
    <div class="panel-heading">
        <h4 class="panel-title">Searching</h4>
    </div>
    <div class="panel-body">
        {!! Form::open(array('method'=>'get','class'=>'form-filter form-inline')) !!}
        <ul class="search">
            <li>
                <div class='form-group'>
                    {!! Form::label('filter[fullname][value]', 'Fullname',array('class'=>'control-label')) !!} : 
                    {!! Form::text('filter[fullname][value]',Input::get('filter.fullname.value'),array('class'=>'form-control')) !!}{!! Form::hidden('filter[fullname][type]','7') !!}
                </div>
            </li>
            <li>
                <div class='form-group'>
                    {!! Form::label('filter[email][value]', 'Email',array('class'=>'control-label')) !!} : 
                    {!! Form::text('filter[email][value]',Input::get('filter.email.value'),array('class'=>'form-control')) !!}{!! Form::hidden('filter[email][type]','7') !!}
                </div>
            </li>
            <li>
                <div class='form-group'>
                    <br/>
                    <button class="btn btn-success">Submit</button>
                </div>
            </li>
        </ul>
        {!! Form::close() !!}
    </div>
</div>
