@extends($theme)

@section('stepLink')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{ route($moduleTitleP.'.index') }}">Admins</a></li>
</ol>
@endsection

@section('content')
<div class="container-fluid set">
    <div class="col-md-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>Manage {{ ucwords($moduleTitleP) }}
                        <div class="pull-right">
                            <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#create-{{ $moduleTitleS }}"><i aria-hidden="true" class="fa fa-plus"></i> Create New {{ ucwords($moduleTitleS) }}</button>
                            <button class="btn btn-sm btn-info search-modules"><i aria-hidden="true" class="fa fa-search"></i> Search</button>
                            @include($moduleTitleP.'.create')
                        </div>
                    </div>
                    @include($moduleTitleP.'.search')
                    <div class="card-block">
                        <table class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th width="60px">No</th>
                                    <th>Name</th>
                                    <th>E-mail</th>
                                    <th>Date</th>
                                    <th width="180px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @include($moduleTitleP.'.data')
                                
                            </tbody>
                        </table>
                        @if(!empty($data) && $data->count())
                        {!! $data->appends(Input::all())->render() !!}
                        @endif
</div>
</div>
</div>
<!--/.col-->
</div>
</div>

</div>
@endsection