<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.home') }}"><i class="icon-speedometer"></i> Dashboard</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ URL('admin/admins') }}"><i class="icon-user"></i> Manage Admin</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ URL('admin/users') }}"><i class="icon-people"></i> Manage Users</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ URL('admin/gym') }}"><i class="icon-badge"></i> Manage Gyms</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ URL('admin/sportcategories') }}"><i class="icon-grid "></i>Sports Category</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ URL('admin/tagsports') }}"><i class="icon-tag"></i>Tag Sports</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ URL('admin/musclesgroups') }}"><i class="icon-ghost"></i>Musclesgroups</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ URL('admin/workouts') }}"><i class="icon-fire"></i>Workouts</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ URL('admin/supplements') }}"><i class="icon-star"></i>Supplements</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ URL('admin/language') }}"><i class="icon-star"></i>Language</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.errorManager') }}"><i class="icon-star"></i>Error Message</a>
            </li>
        </ul>
    </nav>
</div>