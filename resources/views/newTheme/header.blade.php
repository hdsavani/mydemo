
        <button class="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button">☰</button>
        <a class="navbar-brand" href="#">Gym Hunt</a>
        <ul class="nav navbar-nav d-md-down-none">
            <li class="nav-item">
                <a class="nav-link navbar-toggler sidebar-toggler" href="#">☰</a>
            </li>

        </ul>
        <ul class="nav navbar-nav ml-auto">
            
            
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="margin-right: 15px">
                    <img src="/newTheme/img/ld.png" class="img-avatar" alt="{{ auth()->user()->email }}">
                    <span class="d-md-down-none">{{ auth()->user()->fullname }}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">

                    <a class="dropdown-item" href="{{ route('admin.logout') }}"><i class="fa fa-lock"></i> Logout</a>
                </div>
            </li>

        </ul>