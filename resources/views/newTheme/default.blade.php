<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,Angular 2,Angular4,Angular 4,jQuery,CSS,HTML,RWD,Dashboard,React,React.js,Vue,Vue.js">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>{{ $projectTitle }}</title>

    <!-- Icons -->
    <link href="{{asset('newTheme/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('newTheme/css/simple-line-icons.css')}}" rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="{{asset('newTheme/css/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('newTheme/css/custom.css')}}" />

    <!-- Bootstrap and necessary plugins -->
    <script src="{{asset('newTheme/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('newTheme/bower_components/tether/dist/js/tether.min.js')}}"></script>
    <script src="{{asset('newTheme/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('newTheme/bower_components/pace/pace.min.js')}}"></script>

    <script type="text/javascript">
        var current_page_url = "<?php echo URL::current(); ?>";
        var current_page_fullurl = "<?php echo URL::full(); ?>";
    </script>

</head>


<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
    <header class="app-header navbar">
    @include('newTheme.header')
    </header>

    <div class="app-body">
    @include('newTheme.sidebar')

        <!-- Main content -->
        <main class="main">
            @yield('stepLink')
            <div class="container-fluid curd">
                @yield('content')
            </div>
                
            
        </main>

        <aside class="aside-menu">
            @include('newTheme.footer')
        </aside>


    </div>

    <footer class="app-footer" style="margin-bottom:-20px">
        <a href="http://coreui.io">Gym</a> © 2017.
        <span class="float-right">Powered by <a href="http://coreui.io">Lastindynamics</a>
        </span>
    </footer>

    @include('newTheme.alert')

    <!-- Plugins and scripts required by all views -->
    <script src="{{asset('newTheme/bower_components/chart.js/dist/Chart.min.js')}}"></script>


    <!-- GenesisUI main scripts -->
    <script src="{{asset('newTheme/js/app.js')}}"></script>s

    <!-- Custom scripts required by this view -->
    <script src="{{asset('newTheme/js/views/main.js')}}"></script>

    <script src="{{asset('newTheme/js/bootbox.js')}}"></script>
    <script src="{{asset('newTheme/js/jquery.form.min.js')}}"></script>
    <script src="{{asset('newTheme/js/crud.js')}}"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script type="text/javascript">
        $select = $(".js-example-basic-multiple").select2();
        $select.on("select2:select", function (e) { 
            var args = JSON.stringify(e.params, function (key, value) {
              if (value && value.nodeName) return "[DOM node]";
              if (value instanceof $.Event) return "[$.Event]";

              if(key == 'id'){
                id = value;
              }
              if(key == 'text'){
                text = value;
              }

              return value;
            });

            $(this).parents("form").find(".fav-tags").append("<tr class='my-fav fav-"+id+"'><td>"+text+"</td><td><input type='checkbox' name='fav["+id+"]' value='"+id+"'></td></tr>");
        });

        $select.on("select2:unselect", function (e) { 
            var args = JSON.stringify(e.params, function (key, value) {
              if (value && value.nodeName) return "[DOM node]";
              if (value instanceof $.Event) return "[$.Event]";

              if(key == 'id'){
                id = value;
              }

              return value;
            });

            $(this).parents("form").find(".fav-tags").find(".fav-"+id).remove();
        });

        
    </script>


    <script type="text/javascript">
        $("body").on("change",".my-fav input[type='checkbox']",function(){
            var count=0;
            $(this).parents("form").find(".my-fav input:checkbox:checked").each(function() {
                count++;
                if(count > 5){
                    alert("You can maximum select 5 favorite");
                }
            });
            if(count > 5){
                $(this).prop("checked",false);
            }
        });
    </script>

    @yield('script')


</body>

</html>