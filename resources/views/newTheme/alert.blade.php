<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">


<script>


  @if(Session::has('success'))

        toastr.success("{{ Session::get('success') }}");
    <?php Session::forget('success');?>
  @endif


  @if(Session::has('info'))

        toastr.info("{{ Session::get('info') }}");
    <?php Session::forget('info');?>
  @endif


  @if(Session::has('warning'))

        toastr.warning("{{ Session::get('warning') }}");
    <?php Session::forget('warning');?>
  @endif


  @if(Session::has('error'))

        toastr.error("{{ Session::get('error') }}");
    <?php Session::forget('error');?>
  @endif


</script>