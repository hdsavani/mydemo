<!-- Modal -->
<div class="modal fade" id="{{ $moduleTitleS }}-{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            {!! Form::model($value, ['method' => 'PATCH','route' => ["$moduleTitleP.update", $value->id]]) !!}
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Edit {{ ucwords($moduleTitleS) }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Name:</strong>
                                {!! Form::text('name', Input::get('name'), array('placeholder' => 'Name','class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Select Sports Category:</strong>
                                {!! Form::select('sportcategory_id',$sportsid,null,['class'=>'form-control','placeholder'=>'Select Sports Category']) !!}
                                
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="pull-right">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary create-crud">Submit</button>
                     </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>