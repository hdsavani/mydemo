@extends($theme)
@section('stepLink')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Dashboard / </a><a href="{{ route('admin.errorManager') }}">Error Message</a></li>
    
</ol>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Manage Error Message
                </div>
                
                <div class="card-block">
                    <div class="header">
                        <div class="row">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="col-md-2 text-right" style="padding-top: 10px">
                                <strong>Select Language:</strong>
                            </div>
                            <div class="col-md-4">
                                {!! Form::select('language', array('' => 'Default English') + $languages, null, array('class' => 'form-control language-dropdown')) !!}
                                <br>
                            </div>
                            <div class="row-edit-content cancel" style="display:none">
                                <div class="row cancel-remove">
                                    <div class="col-md-4">
                                        {!! Form::text('value', null, array('class' => 'form-control value-edit-name','placeholder'=>'Value')) !!}
                                    </div>
                                    <div class="col-md-4">
                                        <button class="btn btn-success save-content">Save</button>
                                        <button class="btn btn-danger cancel-content">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-manage">
                        <table class="table table-bordered table-striped table-sm dis-content">
                            <thead>
                                <tr>
                                    <th width="100px">Code</th>
                                    <th>Error Message</th>
                                    <th width="100px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($contentType) && $contentType->count())
                                @foreach($contentType as $val)
                                <tr>
                                    <td data-id-def="{{ $val->code }}" class="name-head">{{ $val->code }}</td>
                                    <td data-id="{{ $val->code }}">{{ $val->description }}</td>
                                    <td>
                                        <button class="btn btn-primary edit-content-td">Edit</button>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            
                        </table>
                        
                    </div>
                    @if(!empty($data) && $data->count())
                    {!! $data->appends(Input::all())->render() !!}
                    @endif
                </div>
            </div>
        </div>
        <!--/.col-->
    </div>
</div>
<script src="/newTheme/js/contentManager.js"></script>
@endsection