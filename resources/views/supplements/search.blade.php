<div class="panel panel-default filter-panel filter-panel-custom {{Input::get("filter",false)?'filter-in':'filter-out'}}">
    <div class="panel-heading">
        <h4 class="panel-title">Searching</h4>
    </div>
    <div class="panel-body">
        {!! Form::open(array('method'=>'get','class'=>'form-filter form-inline')) !!}
        <ul class="search">
            <li>
                <div class='form-group'>
                    {!! Form::label('filter[name][value]', 'Name :',array('class'=>'control-label my-label')) !!} 
                    {!! Form::text('filter[name][value]',Input::get('filter.name.value'),array('class'=>'form-control')) !!}{!! Form::hidden('filter[name][type]','7') !!}
                </div>
            </li>
            <li>
                <div class='form-group'>
                    {!! Form::label('filter[type][value]', 'Type :',array('class'=>'control-label my-label')) !!} 
                    {!! Form::text('filter[type][value]',Input::get('filter.type.value'),array('class'=>'form-control')) !!}{!! Form::hidden('filter[type][type]','7') !!}
                </div>
            </li>
            
            <li>
                <div class='form-group'>
                    <br/>
                    <button class="btn btn-success">Search</button>
                </div>
            </li>
        </ul>
        {!! Form::close() !!}
    </div>
</div>
