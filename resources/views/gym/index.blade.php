@extends($theme)

@section('stepLink')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{ route($moduleTitleP.'.index') }}">Gyms</a></li>
</ol>
@endsection

@section('content')
<style type="text/css">
    .header-fixed .app-header{
        z-index: 10;
    }
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Manage {{ ucwords($moduleTitleP) }}
                    
                    <div class="pull-right">
                        <button class="btn btn-sm btn-success create-gym" data-toggle="modal" data-target="#create-{{ $moduleTitleS }}"><i aria-hidden="true" class="fa fa-plus"></i> Create New {{ ucwords($moduleTitleS) }}</button>
                        <button class="btn btn-sm btn-info search-modules"><i aria-hidden="true" class="fa fa-search"></i> Search</button>
                        @include($moduleTitleP.'.create')
                        @include($moduleTitleP.'.update')
                    </div>
                </div>
                @include($moduleTitleP.'.search')
                
                <div class="card-block">
                    <div class="header">
                    </div>
                    <table class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th width="50px;">No</th>
                                <th>Picture</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th style="width: 210px">Tag Sports</th>
                                <th>Created date</th>
                                <th width="150px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @include($moduleTitleP.'.data')
                            
                        </tbody>
                    </table>
                @if(!empty($data) && $data->count())
                {!! $data->appends(Input::all())->render() !!}
                @endif
                    
</div>
</div>
</div>
<!--/.col-->
</div>

</div>


@endsection

@section('script')
<link href="{{ asset('adminTheme/css/map.css') }}" rel="stylesheet">
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAztYRxIDDbVvH7WCOXx2OBecHe79PzE4I&v=3.exp&libraries=places"></script>
<script src="{{ asset('adminTheme/js/map.js') }}"></script>

<script type="text/javascript">
    $(".edit-gym").click(function(){
        var m_cl = $(this).attr('my-class');
        var add = $(this).attr('data-address');
        var longitude = $(this).attr('data-longitude');
        var latitude = $(this).attr('data-latitude');
        $("#pac-input-edit").val(add); 
        resizeMap2(latitude, longitude);
        $(".lat_edit").val(latitude);
        $(".lon_edit").val(longitude);
        $(".update-body").html($("."+m_cl).html());
    });

    $(".create-gym").click(function(){
        $("#pac-input").val('Italy');
        resizingMapOne(41.87194, 12.567379999999957);
    });
</script>
@endsection