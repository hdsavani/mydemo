@if(!empty($data) && $data->count())
    @foreach($data as $key => $value)
        <tr>
            <td>{{ ++$i }}</td>
            <td>
                @if(!is_null($value->main_image))
                <img src="{{ $value->main_image }}" style="width:40px;height:40px">
                @else
                <img src="/newTheme/img/avatars/8.jpg" style="width:40px;height:40px">
                @endif
            </td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->email }}</td>
            <td style="width: 210px">{{ $value->tags }}</td>
            <td>{{ myDateFormat('Y-m-d H:i:s', $value->created_at) }}</td>
            <td>
                
                <button class="padding-bt btn btn-sm btn-primary edit-gym" my-class="update-body-content-{{ $value->id }}" data-toggle="modal" data-address="{{ $value->address }}" data-latitude="{{ $value->latitude }}" data-longitude="{{ $value->longitude }}" data-target="#update-gym"><i aria-hidden="true" class="fa fa-edit"></i> Edit</button>
                @include($moduleTitleP.'.edit')
                <button class="padding-bt btn btn-sm btn-danger remove-crud" data-id="{{ $value->id }}" data-action="{{ route($moduleTitleP.'.destroy',$value->id) }}"><i aria-hidden="true" class="fa fa-trash-o"></i> Delete</button>
                <a href="{{ route('manage.media',['gym',$value->id]) }}" class="btn btn-info btn-sm" style="margin-top: 3px;"><i class="fa fa-image"></i> Manage Images</a>
            </td>
        </tr>
    @endforeach
@endif