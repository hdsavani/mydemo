<!-- Modal -->
<div class="modal fade" id="create-{{ $moduleTitleS }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Create {{ ucwords($moduleTitleS) }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => $moduleTitleP.'.store','method'=>'POST','autocomplete'=>'off')) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Name:</strong>
                                {!! Form::text('name', Input::get('name'), array('placeholder' => 'Name','class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Email:</strong>
                                {!! Form::text('email', Input::get('email'), array('placeholder' => 'Email','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                            <strong>Telephone No. :</strong>
                                {!! Form::text('tel_number', Input::get('tel_number'), array('placeholder' => 'Telephone No.','class' => 'form-control')) !!}    
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Opening hours:</strong>
                                {!! Form::text('opening_hours', Input::get('opening_hours'), array('placeholder' => 'Opening hours','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    

                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Price:</strong>
                                {!! Form::text('price', Input::get('price'), array('placeholder' => 'Price','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Description:</strong>
                                {!! Form::textarea('description', Input::get('description'), array('placeholder' => 'Description','class' => 'form-control','rows'=>'5')) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <strong>Select Tagsports:</strong>
                            <br/>
                            <select class="js-example-basic-multiple js-states form-control select2-hidden-accessible" name="tags[]" multiple="" tabindex="-1" aria-hidden="true" style="width:400px">
                              @if(!empty($tagsList))
                                @foreach($tagsList as $k => $v)
                                  <optgroup label="{{ $k }}">
                                    @foreach($v as $vsub)
                                    <option value="{{ $vsub->id }}">{{ $vsub->name }}</option>
                                    @endforeach
                                  </optgroup>
                                  @endforeach
                              @endif
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Gym Picture:</strong>
                                {!! Form::file('image', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>

                    <br/>

                    <table class="table table-bordered table-striped table-sm fav-tags">
                        <tr>
                            <th>Tag Name</th>
                            <th width="100px">Is Favorite</th>
                        </tr>
                    </table>

                    <br/>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                        <strong>Address</strong>
                            <div id="map" class="">
                              <input id="pac-input" class="controls" type="text" placeholder="insert the location" name="address">
                              <div id="map-canvas"> </div>
                              <input type="hidden" name="latitude" class="lat" />
                              <input type="hidden" name="longitude" class="lon" />
                            </div>
                        </div>
                    </div>

                    <br/>
                    <div class="pull-right">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary create-crud  create-crud">Submit</button>
                    </div>
                    
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>