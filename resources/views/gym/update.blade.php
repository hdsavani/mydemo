<!-- Modal -->
<div class="modal fade" id="update-gym" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Update {{ ucwords($moduleTitleS) }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => $moduleTitleP.'.gym-update','method'=>'POST','autocomplete'=>'off')) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    
                    <div class="update-body">
                    </div>

                    <br/>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <strong>Address : </strong>
                            <div id="map-edit">
                              <input id="pac-input-edit" class="controls" type="text" placeholder="insert the location" name="address">
                              <div id="map-canvas-edit"> </div>
                              <input type="hidden" name="latitude" class="lat_edit" />
                              <input type="hidden" name="longitude" class="lon_edit" />
                            </div>
                        </div>
                    </div>

                    <br/>
                    <div class="pull-right">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary create-crud  create-crud">Submit</button>
                    </div>
                    
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>