@if(!empty($data) && $data->count())
    @foreach($data as $key => $value)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->groupname }}</td>
            <td><img src="{{ $value->path1 }}" style="width:40px;height:40px"></td>
            <td><img src="{{ $value->path2 }}" style="width:40px;height:40px"></td>
            <td>{{ $value->series }}</td>
            <td>{{ myDateFormat('Y-m-d H:i:s', $value->created_at) }}</td>
            <td>
                <button class="padding-bt btn btn-sm btn-primary" data-toggle="modal" data-target="#{{ $moduleTitleS }}-{{ $value->id }}"><i aria-hidden="true" class="fa fa-edit"></i> Edit</button>
                @include($moduleTitleP.'.edit')
                <button class="padding-bt btn btn-sm btn-danger remove-crud" data-id="{{ $value->id }}" data-action="{{ route($moduleTitleP.'.destroy',$value->id) }}"><i aria-hidden="true" class="fa fa-trash-o"></i> Delete</button>
               
            </td>
        </tr>
    @endforeach
@endif