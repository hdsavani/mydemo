<div class="panel panel-default filter-panel filter-panel-custom {{Input::get("filter",false)?'filter-in':'filter-out'}}">
    <div class="panel-heading">
        <h4 class="panel-title">Searching</h4>
    </div>
    <div class="panel-body">
        {!! Form::open(array('method'=>'get','class'=>'form-filter form-inline')) !!}
        <ul class="search">
            <li>
                <div class='form-group'>
                    {!! Form::label('filter[name][value]', 'Name :',array('class'=>'control-label my-label')) !!} 
                    {!! Form::text('filter[name][value]',Input::get('filter.name.value'),array('class'=>'form-control')) !!}{!! Form::hidden('filter[name][type]','7') !!}
                </div>
            </li>
            <li>
                <div class='form-group'>
                    {!! Form::label('filter[name][value]', 'Select Musclesgroup :',array('class'=>'control-label my-label')) !!}
                    {!! Form::select('filter[id_musclesgroup][value]',$musclesgrouplist,null,['class'=>'form-control','placeholder'=>'Select  Musclesgroup']) !!}{!! Form::hidden('filter[id_musclesgroup][type]','1') !!}
                </div>
            </li>
            
            <li>
                <div class='form-group'>
                    <br/>
                    <button class="btn btn-success">Submit</button>
                </div>
            </li>
        </ul>
        {!! Form::close() !!}
    </div>
</div>
