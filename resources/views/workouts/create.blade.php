<!-- Modal -->
<div class="modal fade" id="create-{{ $moduleTitleS }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Create {{ ucwords($moduleTitleS) }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => $moduleTitleP.'.store','method'=>'POST','autocomplete'=>'off')) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Select Musclesgroup :</strong>
                                {!! Form::select('id_musclesgroup',$musclesgrouplist,'',['class' => 'form-control','placeholder'=>'Select Musclesgroup']) !!}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Name :</strong>
                                {!! Form::text('name', Input::get('name'), array('placeholder' => 'Name','class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                    </div>
                        
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>First Image :</strong>
                                {!! Form::file('path1', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Second Image :</strong>
                                {!! Form::file('path2', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Difficulty :</strong>
                                {!! Form::text('difficulty', Input::get('difficulty'), array('placeholder' => 'Difficulty','class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Description :</strong>
                                {!! Form::text('description', Input::get('description'), array('placeholder' => 'Description','class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                    </div>
                   
                   <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Repetition :</strong>
                                {!! Form::text('repetition', Input::get('repetition'), array('placeholder' => 'Repetition','class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Series :</strong>
                                {!! Form::text('series', Input::get('series'), array('placeholder' => 'Series','class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                    </div>

                    <br/>
                    <div class="pull-right">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary create-crud create-crud">Submit</button>
                        
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>