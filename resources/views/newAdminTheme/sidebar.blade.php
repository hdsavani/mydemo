
			<div class="current-user">
				<a href="#" class="name">
					<img class="avatar" src="/newAdminTheme/images/avatars/1.jpg" />
					<span>
						{{ auth()->user()->fullname }}
						<i class="fa fa-chevron-down"></i>
					</span>
				</a>
				<ul class="menu">
					<li>
						<a href="account-profile.html">Account settings</a>
					</li>
					<li>
						<a href="{{ route('admin.logout') }}">Log out</a>
					</li>
				</ul>
			</div>
			<div class="menu-section">
				<h3>General</h3>
				<ul>
					<li>
						<a href="{{route('admin.home')}}" class="active">
							<i class="ion-android-earth"></i> 
							<span>Dashboard</span>
						</a>
					</li>
					<li>
						<a href="{{ URL('admin/users') }}">
							<i class="icon-people"></i> 
							<span>Manage Users</span>
						</a>
					</li>
					<li>
						<a href="{{ URL('admin/admins') }}">
							<i class="ion-person-stalker"></i> 
							<span>Admins</span>
						</a>
					</li>
					
				</ul>
			</div>
			