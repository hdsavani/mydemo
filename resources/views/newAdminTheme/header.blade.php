<div class="menubar">
	<div class="sidebar-toggler visible-xs">
		<i class="ion-navicon"></i>
	</div>

	<div class="page-title">
		@if(isset($moduleTitleS))
			Manage {{ ucwords($moduleTitleS) }}
		@else
			Dashboard
		@endif
	</div>			
</div>
