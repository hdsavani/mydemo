<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />	
	<title>Wolf - Bootstrap Admin Theme</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!-- stylesheets -->
	<link rel="stylesheet" type="text/css" href="{{ asset('newAdminTheme/css/compiled/theme.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('newAdminTheme/css/vendor/animate.css" ')}}/>
	<link rel="stylesheet" type="text/css" href="{{ asset('newAdminTheme/css/vendor/brankic.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('newAdminTheme/css/vendor/ionicons.min.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('newAdminTheme/css/vendor/font-awesome.min.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('newAdminTheme/css/vendor/datepicker.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('newAdminTheme/css/vendor/morris.css')}}" />

	<!-- javascript -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script src="{{asset('newAdminTheme/js/bootstrap/bootstrap.min.js')}}"></script>
	<script src="{{asset('newAdminTheme/js/vendor/jquery.cookie.js')}}"></script>
	<script src="{{asset('newAdminTheme/js/vendor/moment.min.js')}}"></script>
	<script src="{{asset('newAdminTheme/js/theme.js')}}"></script>
	<script src="{{asset('newAdminTheme/js/vendor/bootstrap-datepicker.js')}}"></script>
	<script src="{{asset('newAdminTheme/js/vendor/raphael-min.js')}}"></script>
	<script src="{{asset('newAdminTheme/js/vendor/morris.min.js')}}"></script>

	<script src="{{asset('newAdminTheme/js/vendor/jquery.flot/jquery.flot.js')}}"></script>
	<script src="{{asset('newAdminTheme/js/vendor/jquery.flot/jquery.flot.time.js')}}"></script>
	<script src="{{asset('newAdminTheme/js/vendor/jquery.flot/jquery.flot.tooltip.js')}}"></script>
</head>
	
<body id="dashboard">
	<div id="wrapper">
	
		<div id="sidebar-default" class="main-sidebar">
			@include('newAdminTheme.sidebar')
		</div>

		<div id="content">	
		@include('newAdminTheme.header')
			<div class="content-wrapper">
				@yield('content')
			</div>
		</div>
		
	</div>
		@include('newAdminTheme.footer')
</body>
</html>