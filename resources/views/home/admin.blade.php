@extends($theme)

@section('content')
<div class="container">
	<br/>
	<h1>Welcome to Dashboard</h1>
	<br/>
	<div class="row">
        <div class="col-sm-6 col-lg-3">
            <div class="card card-inverse card-primary">
                <div class="card-block pb-0">
                    <div class="btn-group float-right">
                        <button type="button" class="btn btn-transparent active dropdown-toggle p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-settings"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                    <h4 class="mb-0">{{ $totalusers }}</h4>
                    <p>Total Users</p>   
                </div>
                <div class="chart-wrapper px-3" style="height:70px;"><iframe class="chartjs-hidden-iframe" style="display: block; overflow: hidden; border: 0px none; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;" tabindex="-1"></iframe>
                    <canvas id="card-chart1" class="chart" height="70" style="display: block; width: 217px; height: 70px;" width="217"></canvas>
                </div>
            </div>  
        </div>
        <!--/.col-->

        <div class="col-sm-6 col-lg-3">
            <div class="card card-inverse card-info">
                 <div class="card-block pb-0">
                    <button type="button" class="btn btn-transparent active p-0 float-right">
                        <i class="icon-location-pin"></i>
                    </button>
                    <h4 class="mb-0">{{ $totalgyms }}</h4>
                    <p>Total Gyms</p>
                </div>
                <div class="chart-wrapper px-3" style="height:70px;"><iframe class="chartjs-hidden-iframe" style="display: block; overflow: hidden; border: 0px none; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;" tabindex="-1"></iframe>
                    <canvas id="card-chart2" class="chart" height="70" style="display: block; width: 217px; height: 70px;" width="217"></canvas>
                </div>
            </div>
        </div>
        <!--/.col-->

        <div class="col-sm-6 col-lg-3">
            <div class="card card-inverse card-warning">
                <div class="card-block pb-0">
                    <div class="btn-group float-right">
                        <button type="button" class="btn btn-transparent active dropdown-toggle p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-settings"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                    <h4 class="mb-0">{{ $totaltagsports }}</h4>
                    <p>Total Tagsports</p>
                </div>
                <div class="chart-wrapper" style="height:70px;"><iframe class="chartjs-hidden-iframe" style="display: block; overflow: hidden; border: 0px none; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;" tabindex="-1"></iframe>
                    <canvas id="card-chart3" class="chart" height="70" style="display: block; width: 249px; height: 70px;" width="249"></canvas>
                </div>
            </div>
        </div>
        <!--/.col-->

        <div class="col-sm-6 col-lg-3">
            <div class="card card-inverse card-danger">
                <div class="card-block pb-0">
                    <div class="btn-group float-right">
                        <button type="button" class="btn btn-transparent active dropdown-toggle p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-settings"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                    <h4 class="mb-0">{{ $totalworkouts }}</h4>
                    <p>Total Workouts</p>
                </div>
                <div class="chart-wrapper px-3" style="height:70px;"><iframe class="chartjs-hidden-iframe" style="display: block; overflow: hidden; border: 0px none; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;" tabindex="-1"></iframe>
                    <canvas id="card-chart4" class="chart" height="70" style="display: block; width: 217px; height: 70px;" width="217"></canvas>
                </div>
            </div>
        </div>
        <!--/.col-->
    </div>
</div>
@endsection