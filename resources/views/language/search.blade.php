<div class="panel panel-default filter-panel filter-panel-custom {{Input::get("filter",false)?'filter-in':'filter-out'}}">
    <div class="panel-heading">
        <h4 class="panel-title">Searching</h4>
    </div>
    <div class="panel-body">
        {!! Form::open(array('method'=>'get','class'=>'form-filter form-inline')) !!}
        <ul class="search">
            <li>
                <div class='form-group'>
                    {!! Form::label('filter[name][value]', 'Language :',array('class'=>'control-label my-label')) !!} 
                    {!! Form::text('filter[language][value]',Input::get('filter.language.value'),array('class'=>'form-control')) !!}{!! Form::hidden('filter[language][type]','7') !!}
                </div>
            </li>
          
            <li>
                <div class='form-group'>
                    <br/>
                    <button class="btn btn-success">Search</button>
                </div>
            </li>
        </ul>
        {!! Form::close() !!}
    </div>
</div>
