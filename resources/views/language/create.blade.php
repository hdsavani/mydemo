<!-- Modal -->
<div class="modal fade" id="create-{{ $moduleTitleS }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Create {{ ucwords($moduleTitleP) }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => $moduleTitleP.'.store','method'=>'POST','autocomplete'=>'off')) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Language :</strong>
                                {!! Form::text('language', Input::get('language'), array('placeholder' => 'Language','class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                        
                    </div>
                   
                    <br/>
                    <div class="pull-right">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary create-crud create-crud">Submit</button>
                        
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>