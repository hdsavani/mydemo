@extends($theme)

@section('stepLink')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{ route($type.'.index') }}">{{ ucwords($type) }}</a></li>
</ol>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Manage {{ ucwords($moduleTitleP) }}
                    
                    <div class="pull-right">
                        <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#create-{{ $moduleTitleS }}"><i aria-hidden="true" class="fa fa-plus"></i> Create New {{ ucwords($moduleTitleS) }}</button>
                        <a href="{{ route($type.'.index') }}" class="btn btn-sm btn-info"><i aria-hidden="true" class="fa fa-left"></i> Back</a>
                        @include($moduleTitleP.'.create')
                    </div>
                </div>
                <div class="card-block">
                    <div class="header">
                    </div>
                    <table class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th width="80px">No</th>
                                <th>Image</th>
                                <th width="250px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($data) && $data->count())
                                @foreach($data as $key => $value)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>
                                            @if(!is_null($value->path))
                                            <img src="{{ $value->path }}" style="width:50px;height:50px">
                                            @else
                                            <img src="/newTheme/img/avatars/8.jpg" style="width:50px;height:50px">
                                            @endif
                                        </td>
                                        <td>
                                            <button class="padding-bt btn btn-sm btn-danger remove-crud" data-id="{{ $value->id }}" data-action="{{ route('manageMedia.destroy',[$type,$value->id]) }}"><i aria-hidden="true" class="fa fa-trash-o"></i> Delete</button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <!--/.col-->
    </div>
    @if(!empty($data) && $data->count())
    {!! $data->appends(Input::all())->render() !!}
    @endif
</div>
@endsection