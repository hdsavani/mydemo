@extends('adminTheme.login')
@section('content')
    <div class="container">
        <h1 class="logo-text"><strong>Gym Hunt</strong></h1>
        <br/>
    <div id="login-box">

        <div class="logo">
            <img src="/newTheme/img/logo.png" class="img img-responsive center-block"/>
        </div>
        <div class="controls">
            <p id="profile-name" class="profile-name-card"></p>
            {!! Form::open(array('route' => 'admin.login','class'=>'form-signin')) !!}
                <span id="reauth-email" class="reauth-email"></span>
                <label class="panel-login">
                        <div class="login_result">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger" style="width:100%">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </label>
                    <label class="email">Email - Address : </label>
                {!! Form::text('email', Input::get('email'), array('placeholder' => 'Email','class' => 'form-control','id'=>'inputEmail')) !!}
                <br>
                <label class="password">Password :</label>
                {!! Form::password('password', array('placeholder' => 'Password','class'=>'form-control','id'=>'inputPassword')) !!}
                
                </div><br>
                <button class="btn btn-lg btn-primary btn-block btn-custom" type="submit">Sign in</button>
            
            {!! Form::close() !!}
        </div><!-- /.controls -->
    </div><!-- /#login-box -->
</div><!-- /.container -->
@endsection