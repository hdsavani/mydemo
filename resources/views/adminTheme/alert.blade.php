@if ($errors->any())
<script type="text/javascript">
	$.notify({
     icon: 'pe-7s-gift',
     message: "<strong>Error Alert</strong> <br> Please check the form below for errors"

    },{
        type: 'error',
        timer: 4000
    });
</script>
@endif

@if ($message = Session::get('success'))
<script type="text/javascript">
	$.notify({
     icon: 'pe-7s-gift',
     message: "<strong>Success Alert</strong> <br>"+'<?php echo $message; ?>'

    },{
        type: 'success',
        timer: 4000
    });
</script>
<?php Session::forget('success');?>
@endif


@if ($message = Session::get('error'))
<script type="text/javascript">
	$.notify({
     icon: 'pe-7s-gift',
     message: "<strong>Error Alert</strong><br/>"+"<?php echo $message; ?>"

    },{
        type: 'error',
        timer: 4000
    });
</script>

<?php Session::forget('error');?>
@endif

@if ($message = Session::get('warning'))
<script type="text/javascript">
	$.notify({
     icon: 'pe-7s-gift',
     message: "<strong>Warning Alert</strong> <br>"+'<?php echo $message; ?>'

    },{
        type: 'warning',
        timer: 4000
    });
</script>
<?php Session::forget('warning');?>
@endif

@if ($message = Session::get('info'))
<script type="text/javascript">
	$.notify({
     icon: 'pe-7s-gift',
     message: "<strong>Information Alert</strong> <br>"+'<?php echo $message; ?>'

    },{
        type: 'info',
        timer: 4000
    });
</script>
<?php Session::forget('info');?>
@endif