<?php 
    $currentPageURL = URL::current(); 
    $pageArray = explode('/', $currentPageURL);
    $pageActive = isset($pageArray[4]) ? $pageArray[4] : 'test';
?>

<div class="sidebar" data-color="purple" data-image="/adminTheme/img/sidebar-5.jpg">
	<div class="sidebar-wrapper">
        <div class="logo">
            <a href="http://www.creative-tim.com" class="simple-text">
                {{ $projectTitle }}
            </a>
        </div>

        <ul class="nav">
            <li class="{{ $pageActive == 'home' ? 'active' : ''  }}">
                <a href="{{ route('admin.home') }}">
                    <i class="pe-7s-graph"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="{{ $pageActive == 'admins' ? 'active' : ''  }}">
                <a href="{{ route('admins.index') }}">
                    <i class="pe-7s-user"></i>
                    <p>Manage Admins</p>
                </a>
            </li>
            <li class="{{ $pageActive == 'users' ? 'active' : ''  }}">
                <a href="{{ route('users.index') }}">
                    <i class="pe-7s-user"></i>
                    <p>Manage Users</p>
                </a>
            </li>
            <li class="{{ $pageActive == 'hashtags' ? 'active' : ''  }}">
                <a href="{{ route('hashtags.index') }}">
                    <i class="pe-7s-science"></i>
                    <p>Manage Hashtags</p>
                </a>
            </li>
            <li class="{{ $pageActive == 'events' ? 'active' : ''  }}">
                <a href="{{ route('events.index') }}">
                    <i class="pe-7s-science"></i>
                    <p>Manage Events</p>
                </a>
            </li>
            <li class="active-pro">
                <a href="{{ route('admin.logout') }}">
                    <i class="pe-7s-rocket"></i>
                    <p>Logout</p>
                </a>
            </li>
        </ul>
	</div>
</div>