<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Light Bootstrap Dashboard by Creative Tim</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('adminTheme/css/bootstrap.min.css') }}" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="{{ asset('adminTheme/css/animate.min.css') }}" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="{{ asset('adminTheme/css/light-bootstrap-dashboard.css')}}" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{ asset('adminTheme/css/demo.css')}}" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{ asset('adminTheme/css/pe-icon-7-stroke.css')}}" rel="stylesheet" />
    <link href="{{ asset('adminTheme/css/custom.css')}}" rel="stylesheet" />


     <!--   Core JS Files   -->
    <script src="{{ asset('adminTheme/js/jquery-1.10.2.js')}}" type="text/javascript"></script>

    <style type="text/css">
        #process_img{
            opacity: 0.7;
            position: fixed;
            text-align: center;
            top: 0;
            width: 80%;
            z-index: 1001;
            display: none;
            background: white;
            height: 100%;
        }
        #process_img img{
            position: relative;
            top: 40%;
        }
    </style>

    <script type="text/javascript">
        var current_page_url = "<?php echo URL::current(); ?>";
        var current_page_fullurl = "<?php echo URL::full(); ?>";
    </script>

    <script src="{{ asset('adminTheme/js/bootstrap.min.js')}}" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="{{ asset('adminTheme/js/bootstrap-checkbox-radio-switch.js')}}"></script>

    <!--  Charts Plugin -->
    <script src="{{ asset('adminTheme/js/chartist.min.js')}}"></script>

    <!--  Notifications Plugin    -->
    <script src="{{ asset('adminTheme/js/bootstrap-notify.js')}}"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="{{ asset('adminTheme/js/light-bootstrap-dashboard.js')}}"></script>
    <script src="{{ asset('adminTheme/js/bootbox.js') }}"></script>
    <script src="{{ asset('adminTheme/js/jquery.form.min.js') }}"></script>

    

</head>
<body>

<div class="wrapper">
    @include('adminTheme.sidebar')

    <div class="main-panel">

        @include('adminTheme.navbar')

        <div class="content" style="padding: 0px;">
            
            @yield('content')
        </div>

        @include('adminTheme.footer')

    </div>
</div>

<div id="process_img" style="display: none;">
    <img src="/adminTheme/img/loading.gif"/>
</div>

@include('adminTheme.alert')

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script src="{{ asset('adminTheme/js/demo.js')}}"></script>
    <script src="{{ asset('adminTheme/js/crud.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function(){

            // demo.initChartist();

            // $.notify({
         //     icon: 'pe-7s-gift',
         //     message: "Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for every web developer."

         //    },{
         //        type: 'info',
         //        timer: 4000
         //    });

        });
    </script>

    @yield('content')

</body>

</html>
