<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WorkOuts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workouts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_musclesgroup')->unsigned();
            $table->foreign('id_musclesgroup')->references('id')->on('musclesgroups')->onDelete('cascade');
            $table->string('name',100);
            $table->string('path1',120);
            $table->string('path2',120);
            $table->tinyInteger('difficulty');
            $table->text('description');
            $table->integer('repetition');
            $table->integer('series');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
           Schema::drop("workouts");
    }
}
