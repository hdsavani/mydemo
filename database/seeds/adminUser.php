<?php

use Illuminate\Database\Seeder;

class adminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $create = ['fullname'=>'Admin','email'=>'admin@gmail.com','password'=>bcrypt('123456')];
        DB::table("admins")->insert($create);
    }
}
