<?php

use Illuminate\Database\Seeder;

class CreateErrorCode extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $errorCodes = [
            ['code' => '403', 'description' => "you don't have permission."],
            ['code' => '460', 'description' => 'wrong datatype.'],
            ['code' => '461', 'description' => 'Invalid Email or Password.'],
            ['code' => '462', 'description' => 'the email or the password is missing.'],
            ['code' => '463', 'description' => "the email was already registered."],
            ['code' => '404', 'description' => "404 not found error."],
        	];

        foreach ($errorCodes as $key => $value) {
        	DB::table("errors")->insert($value);
        }
    }
}
