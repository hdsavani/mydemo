<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', array('as'=> 'admin.login', 'uses' => 'Auth\LoginController@login'));
Route::post('/', array('as'=> 'admin.login.post', 'uses' => 'Auth\LoginController@loginPost'));
Route::group(['middleware' => 'auth' ,'prefix' => 'admin'], function () {
	Route::get('home', array('as'=> 'admin.home', 'uses' => 'HomeController@index'));
	Route::get('logout', array('as'=> 'admin.logout', 'uses' => 'Auth\LoginController@logout'));

	Route::get('userUserRevoke/{id}', array('as'=> 'admin.users.revokeuser', 'uses' => 'UserController@revoke'));
	Route::post('userBan', array('as'=> 'admin.users.ban', 'uses' => 'UserController@ban'));

	Route::resource('users', 'UserController');
	Route::resource('admins', 'AdminsController');
	Route::resource('gym','GymController');
	Route::resource('sportcategories','SportsCatController');
	Route::resource('tagsports','TagsportsController');
	Route::resource('hashtags', 'HashtagController');
	Route::resource('events', 'EventController');
	Route::resource('musclesgroups', 'MusclesgroupsController');
	Route::resource('workouts', 'WorkoutsController');
	Route::resource('supplements', 'SupplementsController');
	Route::resource('language', 'LanguageController');
	
	Route::post('gym/gym-update', array('as'=> 'gym.gym-update', 'uses' => 'GymController@gymUpdate'));

	Route::get('errorManager', array('as'=> 'admin.errorManager', 'uses' => 'ContentManagerController@contentManager'));
	Route::post('getLanguageContent', array('as'=> 'admin.getLanguageContent', 'uses' => 'ContentManagerController@getLanguageContent'));
	Route::post('updateContent', array('as'=> 'admin.updateContent', 'uses' => 'ContentManagerController@updateContent'));

	Route::get('manage-images/{type}/{id}',['as'=>'manage.media','uses'=>'ManageImageController@index']);
	Route::post('manageMedia',['as'=>'manageMedia.store','uses'=>'ManageImageController@store']);
	Route::delete('manageMedia/{type}/{id}',['as'=>'manageMedia.destroy','uses'=>'ManageImageController@destroy']);
});

Route::get('get-token/{id}',function($id){
	$user = \App\User::find($id);
    $token =  $user->createToken('MyApp',['user'])->accessToken;
    dd($token);
});