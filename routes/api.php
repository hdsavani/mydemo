<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware'=>['checkHeader','checkDatatype','XSS']], function(){
	Route::get('device','API\AuthController@device');

	Route::group(['middleware'=>['createPretoken','XSS']], function(){
		Route::get('user','API\AuthController@userLogin');
		Route::post('user','API\AuthController@userRegister');
		Route::get('user/fb','API\AuthController@userFBLogin');
	});

});

Route::group(['middleware' => ['auth:api','scope:user','XSS','checkDatatype']], function(){
	Route::post('details', 'API\AuthController@details');
	Route::get('gyms', 'API\GymController@gyms');
	Route::get('gym/{id}', 'API\GymController@gym');
	Route::get('exercises', 'API\MuscleGroupController@exercises');
	Route::get('supplements','API\SupplementController@supplements');
	Route::get('supplement/{id}','API\SupplementController@supplement');
	Route::get('exercise/{id}','API\MuscleGroupController@excerise');
});

