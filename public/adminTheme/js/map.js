var geocoder;
var map;
var infowindow = new google.maps.InfoWindow();
var marker;
var g_err = 0;

function initialize() {

  var markers = [];
  var mapOptions = {
    zoom: 7,
    center: new google.maps.LatLng(42.72, 12.00),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    mapTypeControl: false,
    streetViewControl: false
  };
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  map2 = new google.maps.Map(document.getElementById('map-canvas-edit'), mapOptions);

  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var input2 = document.getElementById('pac-input-edit');
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  map2.controls[google.maps.ControlPosition.TOP_LEFT].push(input2);

  var searchBox = new google.maps.places.SearchBox(input);
  var searchBox2 = new google.maps.places.SearchBox(input2);

  // [START region_getplaces]
  // Listen for the event fired when the user selects an item from the
  // pick list. Retrieve the matching places for that item.
  google.maps.event.addListener(searchBox, 'places_changed', function() {
    var places = searchBox.getPlaces();
    if (places.length == 0) {
      return;
    }
    for (var i = 0, marker; marker = markers[i]; i++) {
      marker.setMap(null);
    }

    // For each place, get the icon, place name, and location.
    markers = [];
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, place; place = places[i]; i++) {
      var image = {
        url: place.icon,
        size: new google.maps.Size(75, 75),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      var marker = new google.maps.Marker({
        map: map,
        icon: image,
        title: place.name,
        position: place.geometry.location
      });
      $('.lat').val(marker.position.lat());
      $('.lon').val(marker.position.lng());
      markers.push(marker);
      bounds.extend(place.geometry.location);
    }

    map.fitBounds(bounds);
  });
  // [END region_getplaces]

  google.maps.event.addListener(searchBox2, 'places_changed', function() {
    var places = searchBox2.getPlaces();
    if (places.length == 0) {
      return;
    }
    for (var i = 0, marker; marker = markers[i]; i++) {
      marker.setMap(null);
    }

    // For each place, get the icon, place name, and location.
    markers = [];
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, place; place = places[i]; i++) {
      var image = {
        url: place.icon,
        size: new google.maps.Size(75, 75),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      var marker = new google.maps.Marker({
        map: map,
        icon: image,
        title: place.name,
        position: place.geometry.location
      });
      $('.lat_edit').val(marker.position.lat());
      $('.lon_edit').val(marker.position.lng());
      markers.push(marker);
      bounds.extend(place.geometry.location);
    }

    map2.fitBounds(bounds);
  });

  // Bias the SearchBox results towards places that are within the bounds of the
  // current map's viewport.
  google.maps.event.addListener(map, 'bounds_changed', function() {
    var bounds = map.getBounds();
    searchBox.setBounds(bounds);
  });

  google.maps.event.addListener(map2, 'bounds_changed', function() {
    var bounds = map2.getBounds();
    searchBox2.setBounds(bounds);
  });
}

google.maps.event.addDomListener(window, 'load', initialize);

$('#create-gym').on('show.bs.modal', function() {
   resizeMap();
})

$('.load-map-create').on('show.bs.modal', function() {
   resizeMap();
})

$('#edit-post').on('show.bs.modal', function() {
   // resizeMap2();
})

function resizeMap() {
   if(typeof map =="undefined") return;
   setTimeout( function(){resizingMap();} , 400);
}

function resizeMap2(lat,lon) {
   if(typeof map2 =="undefined") return;
   setTimeout( function(){resizingMap2(lat,lon);} , 400);
}

function resizingMap() {
   if(typeof map =="undefined") return;
   var center = map.getCenter();
   google.maps.event.trigger(map, "resize");
   map.setCenter(center); 
}

function resizingMap2(lat,lon) {
   if(typeof map2 =="undefined") return;
   var center = map2.getCenter();
   // google.maps.LatLng(21.76447250, 72.15193040);
   google.maps.event.trigger(map2, "resize");
   map2.setCenter(new google.maps.LatLng(lat, lon));
   // map2.setCenter(center);
}
function resizingMapOne(lat,lon) {
   if(typeof map =="undefined") return;
   var center = map.getCenter();
   // google.maps.LatLng(21.76447250, 72.15193040);
   google.maps.event.trigger(map, "resize");
   map.setCenter(new google.maps.LatLng(42.72, 12.00));
   // map2.setCenter(center);
}