<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Config;


class Sportcategorie extends Authenticatable
{
	use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function getData($input)
    {
    	$data = static::select("sportcategories.*");

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where($column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where($column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->orderBy("sportcategories.id","DESC")->paginate(15);	
    }
     public function AddData($input)
    {
        return static::create(array_only($input,$this->fillable));
    }
    public function updateData($id, $input)
    {
        return static::find($id)->update(array_only($input,$this->fillable));
    }
    public function destroyData($id)
    {
        return static::find($id)->delete();
    }

    public function sportsId()
    {
        return static::pluck('name','id')->all();
    }

}
