<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Config;

class UserPicture extends Authenticatable
{
	use Notifiable;

    protected $table = 'user_pictures';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'path'];

    public function getData($id)
    {
    	return static::select("user_pictures.*")->where('user_id',$id)->paginate(10);
    }
    public function AddData($input)
    {
        return static::create($input);
    }

    public function destroyData($id)
    {
        $find = static::find($id);

        ImageUpload::removeFile($find->path);

        return $find->delete();
    }
}
