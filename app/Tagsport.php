<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Config;

class Tagsport extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','sportcategory_id',
    ];

    public function getData($input)
    {
    	$data = static::select("tagsports.*","sportcategories.name as selectname")
                        ->join("sportcategories","sportcategories.id","=","tagsports.sportcategory_id");

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where("tagsports.".$column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where("tagsports.".$column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->orderBy("tagsports.id","DESC")->paginate(15);	
    }
     public function AddData($input)
    {
        return static::create(array_only($input,$this->fillable));
    }
    public function updateData($id, $input)
    {
        return static::find($id)->update(array_only($input,$this->fillable));
    }
    public function destroyData($id)
    {
        return static::find($id)->delete();
    }

    public function getTagsLists()
    {
        $data = static::select("tagsports.*","sportcategories.name as selectname")
                        ->join("sportcategories","sportcategories.id","=","tagsports.sportcategory_id")
                        ->get();

        $result = [];
        if($data->count()){
            foreach ($data as $key => $value) {
                $result[$value->selectname][] = $value;
            }
        }

        return $result;
    }

    public function totalTagsport()
    {
        return static::count();
    }
}
