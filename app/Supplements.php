<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Config;

class Supplements extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','type','path','price','popularity',
    ];

    public function getData($input)
    {
    	$data = static::select("supplements.*");

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where($column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where($column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->orderBy("supplements.id","DESC")->paginate(10);	
    }
     public function AddData($input)
    {
        return static::create(array_only($input,$this->fillable));
    }
    public function updateData($id, $input)
    {  

        $data = static::find($id);

        if(!empty($input['path']))
        {
           ImageUpload::removeFile($data->path);            
        }
        return $data->update(array_only($input,$this->fillable));
    }
    public function destroyData($id)
    {
        $find = static::find($id);

        ImageUpload::removeFile($find->path);

        return $find->delete();
    }
    public function getSupplements()
    {
        return static::select('name','type','path','price','popularity')->get();
    }

    public function suppleId($id)
    {
        return static::select('name','path as image','popularity','price','type')->where('id',$id)->first();
    }
}
