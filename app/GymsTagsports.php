<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Config;

class GymsTagsports extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gym_id','tagsport_id','favorite'
    ];

    public function addData($id, $input)
    {
        foreach ($input['tags'] as $key => $value) {
            $insert = ['gym_id'=>$id, 'tagsport_id'=>$value];
            if(in_array($value, $input['fav'])){
                $insert['favorite'] = 1;
            }
            static::create($insert);
        }
    }

    public function addUpdate($id, $input)
    {
        static::where('gym_id',$id)->delete();
        foreach ($input['tags'] as $key => $value) {
            $insert = ['gym_id'=>$id, 'tagsport_id'=>$value];
            if(in_array($value, $input['fav'])){
                $insert['favorite'] = 1;
            }
            static::create($insert);
        }
    }

    public function getGymTags($id)
    {
        return static::select("tagsports.id","tagsports.name","tagsports.sportcategory_id","sportcategories.name as sportcategory_name")
            ->join("tagsports","gyms_tagsports.tagsport_id","=","tagsports.id")
            ->join("sportcategories","sportcategories.id","=","tagsports.sportcategory_id")
            ->where("gyms_tagsports.gym_id",$id)
            ->get();
    }
}
