<?php
	
	function notificationMsg($type, $message){
		\Session::put($type, $message);
	}

	function putValue($message){
		\Session::put('id', $message);
	}

	function myDateFormat($format, $date)
	{
		if (!empty($date)) {
			return	\Carbon\Carbon::createFromFormat($format, $date)->format('M d Y');
		}else{
			return '';
		}
	}