<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Hash;
use App\Language;
use Validator;


class LanguageController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->language = new Language;
        $this->moduleTitleS = 'language';
        $this->moduleTitleP = 'language';

        view()->share('moduleTitleP',$this->moduleTitleP);
        view()->share('moduleTitleS',$this->moduleTitleS);
    }
    
     public function index(Request $request)
    {
        $data = $this->language->getData($request->all());

        return view($this->moduleTitleP.'.index',compact('data'))
                    ->with('i', ($request->input('page', 1) - 1) * 15);;
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'language' => 'required',
        ]);

        $input = $request->all();
        if ($validator->passes()) {

            $this->language->AddData($input);

            notificationMsg('success',$this->crudMessage('add',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'language' => 'required',
        ]);

        $input = $request->all();
        if ($validator->passes()) {
            
            $this->language->updateData($id,$input);

            notificationMsg('success',$this->crudMessage('update',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }


    public function destroy($id)
    {
        $this->language->destroyData($id);

        notificationMsg('success',$this->crudMessage('delete',$this->moduleTitleS));
        return redirect()->route($this->moduleTitleP.'.index');
    }

}
