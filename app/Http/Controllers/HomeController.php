<?php namespace App\Http\Controllers;

use App\User;
use App\Gym;
use App\Tagsport;
use App\Workouts;
class HomeController extends AdminController
{
	
	public function __construct()
	{
		parent::__construct();

		$this->users = new User;
		$this->gyms = new Gym;
		$this->tagsports = new Tagsport;
		$this->workouts = new Workouts;
	}

	public function index()
	{
		$totalusers = $this->users->totalUsers();
		$totalgyms = $this->gyms->totalGyms();
		$totaltagsports = $this->tagsports->totalTagsport();
		$totalworkouts = $this->workouts->totalWorkouts();
		return view('home.admin',compact('totalusers','totalgyms','totaltagsports','totalworkouts'));
	}

}
