<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Musclesgroups;
use App\Workouts;
use Hash;
use Validator;
use App\ImageUpload;

class WorkoutsController extends AdminController
{
     public function __construct()
    {
        parent::__construct();
        $this->workouts = new Workouts;
        $this->musclesgroups = new Musclesgroups;
        $this->moduleTitleS = 'workouts';
        $this->moduleTitleP = 'workouts';

        view()->share('moduleTitleP',$this->moduleTitleP);
        view()->share('moduleTitleS',$this->moduleTitleS);
    }

    public function index(Request $request)
    {
        $data = $this->workouts->getData($request->all());
        $musclesgrouplist = $this->musclesgroups->getMuscleslist();
        return view($this->moduleTitleP.'.index',compact('data','musclesgrouplist'))
                    ->with('i', ($request->input('page', 1) - 1) * 5);	
    } 

     public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
        	'id_musclesgroup' =>'required',
            'name' => 'required',
            'path1' => 'required',
            'path2' => 'required',
            'difficulty' => 'required|numeric',
            'description' => 'required',
            'repetition' => 'required|numeric',
            'series' => 'required|numeric',
        ]);

        $input = $request->all();
        if ($validator->passes()) {

            if($request->hasFile('path1')){
                $input['path1'] = ImageUpload::upload('/upload/workouts/',$request->file('path1'));
            }

            if($request->hasFile('path2')){
                $input['path2'] = ImageUpload::upload('/upload/workouts/',$request->file('path2'));
            }

            $this->workouts->AddData($input);

            notificationMsg('success',$this->crudMessage('add',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }

     public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
        	'id_musclesgroup' =>'required',
            'name' => 'required',
            'difficulty' => 'required|numeric',
            'description' => 'required',
            'repetition' => 'required|numeric',
            'series' => 'required|numeric',
        ]);

        $input = $request->all();
        if ($validator->passes()) {

            if($request->hasFile('path1')){
                $input['path1'] = ImageUpload::upload('/upload/workouts/',$request->file('path1'));
            }else{
                $input = array_except($input, ['path1']);
            }

            if($request->hasFile('path2')){
                $input['path2'] = ImageUpload::upload('/upload/workouts/',$request->file('path2'));
            }else{
                $input = array_except($input, ['path2']);
            }

            $this->workouts->updateData($id,$input);

            notificationMsg('success',$this->crudMessage('update',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

  
    public function destroy($id)
    {
        $this->workouts->destroyData($id);

        notificationMsg('success',$this->crudMessage('delete',$this->moduleTitleS));
        return redirect()->route($this->moduleTitleP.'.index');
    }

}
