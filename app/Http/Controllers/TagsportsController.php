<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Tagsport;
use App\Sportcategorie;
use Hash;
use Validator;

class TagsportsController extends AdminController
{
    public function __construct()
    {
		parent::__construct();
        $this->tagsport = new Tagsport;
        $this->sportscat = new Sportcategorie;
        $this->moduleTitleS = 'tagsports';
        $this->moduleTitleP = 'tagsports';
        view()->share('moduleTitleP',$this->moduleTitleP);
        view()->share('moduleTitleS',$this->moduleTitleP);
    }   

    public function index(Request $request)
    {
        $data = $this->tagsport->getData($request->all());
		$sportsid = $this->sportscat->sportsId();
        return view($this->moduleTitleP.'.index',compact('data','sportsid'))
                    ->with('i', ($request->input('page', 1) - 1) * 15);
    } 

     public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'sportcategory_id' => 'required',
        ]);

        $input = $request->all();
        if ($validator->passes()) {
            $this->tagsport->AddData($input);

            notificationMsg('success',$this->crudMessage('add',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'sportcategory_id' => 'required',
        ]);

        $input = $request->all();
        if ($validator->passes()) {

            $this->tagsport->updateData($id,$input);

            notificationMsg('success',$this->crudMessage('update',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

     public function destroy($id)
    {
        $this->tagsport->destroyData($id);

        notificationMsg('success',$this->crudMessage('delete',$this->moduleTitleS));
        return redirect()->route($this->moduleTitleP.'.index');
    }
}
