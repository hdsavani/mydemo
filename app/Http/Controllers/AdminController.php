<?php namespace App\Http\Controllers;

use Request;
use Input;
use DB;

class AdminController extends Controller
{
	
	public function __construct()
	{
		view()->share('theme','newTheme.default');
		view()->share('login','adminTheme.login');
		view()->share('projectTitle','Gym Hunt');
		view()->share('header','Goood');
	}

	public function checkHeader(){
		
		$header = Request::server('HTTP_X_ILPATIBOLOHEADCUSTENC');
		if($header == 'ilpatiboloreq'){
			return false;
		}

		return true;
	}

	public function getErrorMessage($type){

		$language = Input::has('language') ? Input::get('language') : NULL;

		
		$message = DB::table("errors")
					->select("code as code","description as msg")
					->where("code",$type)
					->where("language",$language)
					->first();

		if(empty($message)){
			
			$default_message = DB::table("errors")
						->select("code as code","description as msg")
						->where("code",$type)
						->where("language",0)
						->first();

			if(empty($default_message)){
				$msg = array('status'=>array('code'=> 108,'error' => "There are error."));
			}else{
				$msg['status'] = (array) $default_message;	
				$msg['status']['code'] = (int) $msg['status']['code'];	
			}
		}else{
			$msg['status'] = (array) $message;
			$msg['status']['code'] = (int) $msg['status']['code'];
		}

		return $msg;
	}


	public function crudMessage($type, $module)
	{
		switch ($type) {
			case 'add':
				return $module . ' created successfully';
				break;
			case 'delete':
				return $module . ' deleted successfully';
				break;
			case 'update':
				return $module . ' updated successfully';
				break;
			
			default:
				# code...
				break;
		}
	}

	public function getSuccessResult($data)
	{
		$result['status'] = ['code' => 200, 'msg' =>''];
		$result['result'] = $data;
		return $result;
	}

	public function checkErrorValidation($token, $validator)
	{
		if(function_exists('ob_gzhandler')) ob_start('ob_gzhandler');
		else ob_start();
		
		// $this->setUserConfig();
		$result = array();

		if($validator->fails()){
			$result['error'] = $this->getErrorMessage('107');
		}else{
            $user = JWTAuth::toUser($token);
            $id = $user->id;

            if($id){
                $result['id'] = $id;
            }else{
                $result['error'] = $this->getErrorMessage('112');
            }
		}

        return $result;
	}

	public function getUniqueToken()
	{
		$generated = substr(md5(rand()),0,5);
		return bcrypt($generated);
	}
}
