<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Hash;
use Validator;
use App\ImageUpload;
use App\UserPicture;
use App\GymPicture;

class ManageImageController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->user = new User;
        $this->userPicture = new UserPicture;
        $this->gymPicture = new GymPicture;
        $this->moduleTitleS = 'manage-media';
        $this->moduleTitleP = 'manageMedia';

        view()->share('moduleTitleP',$this->moduleTitleP);
        view()->share('moduleTitleS',$this->moduleTitleS);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type, $id, Request $request)
    {
        if($type == 'users'){
            $data = $this->userPicture->getData($id);
        }elseif($type == 'gym'){
            $data = $this->gymPicture->getData($id);
        }

        return view($this->moduleTitleP.'.index',compact('data','type','id'))
                    ->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required'
        ]);

        $input = $request->all();
        if ($validator->passes()) {

            if($input['type'] == 'users'){
                $input['path'] = ImageUpload::upload('/upload/users/',$request->file('image'));
                $input['user_id'] = $input['id'];
                $this->userPicture->AddData($input);
            }else if($input['type'] == 'gym'){
                $input['path'] = ImageUpload::upload('/upload/gym/',$request->file('image'));
                $input['gym_id'] = $input['id'];
                $this->gymPicture->AddData($input);
            }

            notificationMsg('success',$this->crudMessage('add',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($type, $id)
    {
        if($type == 'users'){
            $this->userPicture->destroyData($id);
        }elseif($type == 'gym'){
            $this->gymPicture->destroyData($id);
        }

        notificationMsg('success',$this->crudMessage('delete',$this->moduleTitleS));
        return redirect()->back();
    }

    
}
