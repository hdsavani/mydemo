<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Validator;

class LoginController extends AdminController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        parent::__construct();
    }

    public function login()
    {
       return view('auth.adminLogin',compact('images'));
    }

    public function loginPost(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if ($validator->passes()) {
            if (auth()->attempt(array('email' => $input['email'], 'password' => $input['password'])))
            {
                return redirect()->route('admin.home');
            }else{
                return redirect()->route('admin.login')
                    ->with('error','your username and password are wrong.');
            }
        }

        return redirect()->route('admin.login')
            ->with('errors',$validator->errors());
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->route('admin.login');
    }
}
