<?php namespace App\Http\Controllers;

use Input;
use Request;
use View;
use App\User;
use App\Language;
use App\Error;
use Validator;
use Redirect;
use Response;

class ContentManagerController extends AdminController
{
  protected $user,$language,$appcontent;

  public function __construct(User $user,Language $language,Error $appcontent)
  {
    parent::__construct();
      $this->user = $user;
      $this->language = $language;
      $this->appcontent = $appcontent;

      $this->moduleTitleS = 'error code';
      $this->moduleTitleP = 'error code';

      view()->share('moduleTitleP',$this->moduleTitleP);
      view()->share('moduleTitleS',$this->moduleTitleS);
  }

  public function contentManager()
  {
    $languages = $this->language->getLanlist();
    $contentType = $this->appcontent->defaultAppcontent();
    return view('contentManager.contentManager',compact('languages','contentType'));
  }

    public function getLanguageContent()
    {
        $input = Input::except(array('_token'));
        $data = $this->appcontent->getAppcontent($input);
        $contentType = $this->appcontent->defaultAppcontent();
        return Response::json(array('result'=>$data,'d'=>$contentType));
    }

    public function getAppcontent()
    {
        $input = Input::all();
       $validator = Validator::make($input, ['language' => 'required', 'token' => 'required']);
       
       if($validator->passes()){
           $data = $this->appcontent->getContents($input);
           return Response::json(array('done'=>$data));
       }

       return Response::json($this->getErrorMessage('Validation'));
    }

    public function updateContent(Request $request)
    {
        $input = Input::all();
        $input = Input::except($input,array('_token'));
        $data = $this->appcontent->updateContent($input);
        return Response::json(array('result'=>$data));
    }
}