<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Gym;
use Hash;
use Validator;
use App\ImageUpload;
use App\Tagsport;
use App\GymsTagsports;

class GymController extends AdminController
{
	public function __construct()
    {
		parent::__construct();
        $this->gym = new Gym;
        $this->tagsport = new Tagsport;
        $this->gymsTagsports = new GymsTagsports;
        $this->moduleTitleS = 'gym';
        $this->moduleTitleP = 'gym';

        view()->share('moduleTitleP',$this->moduleTitleP);
        view()->share('moduleTitleS',$this->moduleTitleS);
    }   

    public function index(Request $request)
    {
        $data = $this->gym->getData($request->all());
        $tagsList = $this->tagsport->getTagsLists();

        return view($this->moduleTitleP.'.index',compact('data','tagsList'))
                    ->with('i', ($request->input('page', 1) - 1) * 15);
    } 

    public function store(Request $request)
    {

        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'tel_number' => 'required|numeric',
            'price' => 'required|numeric',
            'opening_hours' => 'required',
            'description' => 'required',
            'address' => 'required',
            'tags'=> 'required'
        ]);

        if ($validator->passes()) {

            if($request->hasFile('image')){
                $input['main_image'] = ImageUpload::upload('/upload/gym/',$request->file('image'));
            }

            $create = $this->gym->AddData($input);

            if(!empty($input['tags'])){
                $this->gymsTagsports->addData($create->id, $input);
            }

            notificationMsg('success',$this->crudMessage('add',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'tel_number' => 'required|numeric',
            'price' => 'required|numeric',
            'opening_hours' => 'required',
            'description' => 'required',
            'address' => 'required',
        ]);

        $input = $request->all();
        if ($validator->passes()) {

            if($request->hasFile('image')){
                $input['main_image'] = ImageUpload::upload('/upload/gym/',$request->file('image'));
            }else{
                $input = array_except($input, ['image']);
            }

            $this->gym->updateData($id,$input);

            if(!empty($input['tags'])){
                $this->gymsTagsports->addUpdate($id, $input);
            }

            notificationMsg('success',$this->crudMessage('update',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function destroy($id)
    {
        $this->gym->destroyData($id);

        notificationMsg('success',$this->crudMessage('delete',$this->moduleTitleS));
        return redirect()->route($this->moduleTitleP.'.index');
    }

    public function gymUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'tel_number' => 'required|numeric',
            'price' => 'required|numeric',
            'opening_hours' => 'required',
            'description' => 'required',
            'address' => 'required',
        ]);

        $input = $request->all();
        $id = $input['id'];
        if ($validator->passes()) {

            if($request->hasFile('image')){
                $input['main_image'] = ImageUpload::upload('/upload/gym/',$request->file('image'));
            }else{
                $input = array_except($input, ['image']);
            }

            $this->gym->updateData($id,$input);

            if(!empty($input['tags'])){
                $this->gymsTagsports->addUpdate($id, $input);
            }

            notificationMsg('success',$this->crudMessage('update',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }
}
