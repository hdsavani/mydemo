<?php namespace App\Http\Controllers\API;

use App\Http\Controllers\AdminController;
use App\Musclesgroups;
use App\Workouts;
use Illuminate\Http\Request;
use Validator;

class MuscleGroupController extends AdminController
{

	public function __construct()
	{
		parent::__construct();
        $this->musclesgroups = new Musclesgroups;
        $this->workouts = new Workouts;

	}

    public function exercises(Request $request)
    {
        if(function_exists('ob_gzhandler')) ob_start('ob_gzhandler');
        else ob_start();

        $data = $this->musclesgroups->exercises();
        return response()->json($this->getSuccessResult($data));
    }
    public function excerise($id)
    {

        $data = $this->workouts->exceriseId($id);

        if(is_null($data)){
            return response()->json($this->getErrorMessage('404'));
        }
        
        return response()->json($this->getSuccessResult($data));
    }
}