<?php namespace App\Http\Controllers\API;

use App\Http\Controllers\AdminController;
use App\Gym;
use App\GymsTagsports;
use Illuminate\Http\Request;
use Validator;

class GymController extends AdminController
{

	public function __construct()
	{
		parent::__construct();
        $this->gym = new Gym;
        $this->gymsTagsports = new GymsTagsports;
	}

    public function gyms(Request $request)
    {
        if(function_exists('ob_gzhandler')) ob_start('ob_gzhandler');
        else ob_start();

        $input = $request->all();

        $validator = Validator::make($input, [
            'lat' => 'required',
            'lon'=>'required'
            ]);

        if($validator->passes()){
            $input['user_id'] = auth()->user()->id;
            $data = $this->gym->gyms($input);
            return response()->json($this->getSuccessResult($data));
        }

        return response()->json($this->getErrorMessage('462'));
    }

    public function gym($id)
    {
        $data = $this->gym->findGym($id);

        if(is_null($data)){
            return response()->json($this->getErrorMessage('404'));
        }
    
        $data->tags = $this->gymsTagsports->getGymTags($id);
        return response()->json($this->getSuccessResult($data));
    }
}