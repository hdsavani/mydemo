<?php namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use App\Supplements;
use Validator;

class SupplementController extends AdminController
{
	public function __construct()
	{
	    parent::__construct();
	    $this->supplements = new Supplements;
	}    
	
	public function supplements()
	{
		if(function_exists('ob_gzhandler')) ob_start('ob_gzhandler');
        else ob_start();

        $data = $this->supplements->getSupplements();
        return response()->json($this->getSuccessResult($data));	
	}
	public function supplement($id)
	{
     	$data = $this->supplements->suppleId($id);

     	 if(is_null($data)){
            return response()->json($this->getErrorMessage('404'));
        }
        
		return response()->json($this->getSuccessResult($data));
	}
}
