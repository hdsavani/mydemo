<?php namespace App\Http\Controllers\API;

use App\Http\Controllers\AdminController;
use App\User;
use Illuminate\Http\Request;
use Validator;
use App\Devices;
use App\UserPicture;
use Illuminate\Support\Facades\Auth;

class AuthController extends AdminController
{

    protected $user;

	public function __construct(User $user)
	{
		parent::__construct();
        $this->user = $user;
        $this->devices = new Devices;
        $this->userPicture = new UserPicture;
	}

    public function device(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
                'uuid' => 'required'
            ]);

        if($validator->fails()){
            $result['error'] = $this->getErrorMessage('462');
            return response()->json($result['error']);   
        }
        $checkDevice = $this->devices->checkUUID($input['uuid']);

        $csrfToken = $this->getUniqueToken();
        $input['pre_token'] = $csrfToken;

        if(!is_null($checkDevice)){
            $this->devices->updateDevice($input);
        }else{
            $this->devices->addDevice($input);
        }

        $res['pre_token'] = $csrfToken;
        return response()->json($this->getSuccessResult($res));
    }

    public function userLogin(Request $request)
    {
        if(function_exists('ob_gzhandler')) ob_start('ob_gzhandler');
        else ob_start();

       $input = $request->all();

        $validator = Validator::make($input, [
            'email' => 'required|email'
            ,'password'=>'required'
            ]);

        if($validator->passes()){
            if(Auth::guard('api-auth')->attempt(['email' => request('email'), 'password' => request('password')])){
                $user = Auth::guard('api-auth')->user();
                $token =  $user->createToken('MyApp',['user'])->accessToken;
                $data = array('token'=>$token,'userid'=>$user->id,'fullname'=>$user->fullname);
                return response()->json($this->getSuccessResult($data));
            }
            else{
                return response()->json($this->getErrorMessage('461'));             
            }
        }

        return response()->json($this->getErrorMessage('462'));

    }

    public function userRegister(Request $request)
    {
        if(function_exists('ob_gzhandler')) ob_start('ob_gzhandler');
        else ob_start();

        $input = $request->all();

        $validator = Validator::make($input, [
            'email' => 'required|email',
            'password'=>'required',
            'fullname' => 'required',
            'gender' => 'required'
            ]);

        if($validator->passes()){
            if(is_null($this->user->checkEmail($input['email']))){
                $user = $this->user->AddData($input);
                $token =  $user->createToken('MyApp',['user'])->accessToken;
                $data = array('token'=>$token,'userid'=>$user->id,'fullname'=>$user->fullname);
                return response()->json($this->getSuccessResult($data));
            }
            else{
                return response()->json($this->getErrorMessage('463'));             
            }

        }

        return response()->json($this->getErrorMessage('462'));
    }

    public function userFBLogin(Request $request)
    {
        $input = $request->all();

        if(function_exists('ob_gzhandler')) ob_start('ob_gzhandler');
        else ob_start();

        $validator = Validator::make($input, [
            'fb_token' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($this->getErrorMessage('462'));
        }

        try {
            
            $facebook = new \Facebook\Facebook(array(
              'appId'  => env('FACEBOOK_APP_ID'),
              'secret' => env('FACEBOOK_APP_SECRET'),
              'scope' => 'publish_actions'
            ));
            
            $facebook->setAccessToken($input['fb_token']);
            $fbDetails = $facebook->api('/me?fields=id,name,age_range,about,gender,picture.width(600)');

            if(!empty($fbDetails)){
                $find = $this->user->findUser($fbDetails);
                if(!is_null($find)){
                    $token = $find->createToken('MyApp',['user'])->accessToken;
                    $img = $find->main_image;
                    return response()->json($this->getSuccessResult(['token'=>$token,'userid'=>$find->id, 'userimage'=>$img]));
                }else{
                    $insert['fb_id'] = $fbDetails['id'];
                    $insert['fullname'] = $fbDetails['name'];

                    if(!empty($fbDetails['gender'])){
                        if($fbDetails['gender'] == 'male'){
                            $insert['gender'] = 0;
                        }elseif($fbDetails['gender'] == 'female'){
                            $insert['gender'] = 1;
                        }else{
                            $insert['gender'] = 2;
                        }
                    }

                    $insert['email'] = str_replace(' ','',$fbDetails['name']).rand(100,4).'@gmail.com';

                    if(!empty($fbDetails['picture']['data'])){
                        $ext = pathinfo($fbDetails['picture']['data']['url'], PATHINFO_EXTENSION);
                        $ext = substr($ext, 0, strrpos($ext, '?'));
                        $ch = curl_init($fbDetails['picture']['data']['url']);

                        $imgName = '/upload/users/'.time().'.'.$ext;
                        $insert['main_image'] = $imgName;

                        $fp = fopen(public_path($imgName), 'wb');
                        curl_setopt($ch, CURLOPT_FILE, $fp);
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_exec($ch);
                        curl_close($ch);
                        fclose($fp);
                    }

                    $create = $this->user->createNewUser($insert);

                    $imagesIds = $facebook->api('/'.$insert['fb_id'].'/photos/uploaded');

                    if(!empty($imagesIds['data'])){
                        foreach ($imagesIds['data'] as $key => $value) {
                            if($key <= 1){  
                                $d = $facebook->api('/'.$value['id'].'?fields=images');
                                $img = $d['images']['0']['source'];

                                if(!empty($img)){
                                    $ext = pathinfo($img, PATHINFO_EXTENSION);
                                    $ext = substr($ext, 0, strrpos($ext, '?'));
                                    $ch = curl_init($img);

                                    $imgName = '/upload/users/'.time().'.'.$ext;

                                    $ins['user_id'] = $create->id;
                                    $ins['path'] = $imgName;

                                    $fp = fopen(public_path($imgName), 'wb');
                                    curl_setopt($ch, CURLOPT_FILE, $fp);
                                    curl_setopt($ch, CURLOPT_HEADER, 0);
                                    curl_exec($ch);
                                    curl_close($ch);
                                    fclose($fp);

                                    $this->userPicture->AddData($ins);
                                }
                            }
                        }
                    }

                    $token = $create->createToken('MyApp',['user'])->accessToken;
                    $img = $create->main_image;
                    
                    return response()->json($this->getSuccessResult(['token'=>$token,'userid'=>$create->id, 'userimage'=>$img]));
                }
            }else{
                return response()->json($this->getErrorMessage('403'));
            }

        } catch (Exception $e) {
            return response()->json($this->getErrorMessage('403'));
        }
    }

	/**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user]);
    }
}