<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Supplements;
use Hash;
use Validator;
use App\ImageUpload;


class SupplementsController extends AdminController
{
     public function __construct()
    {
        parent::__construct();
        $this->supplements = new Supplements;
        $this->imageupload = new ImageUpload;
        $this->moduleTitleS = 'supplements';
        $this->moduleTitleP = 'supplements';

        view()->share('moduleTitleP',$this->moduleTitleP);
        view()->share('moduleTitleS',$this->moduleTitleS);
    }

    public function index(Request $request)
    {
        $data = $this->supplements->getData($request->all());
        return view($this->moduleTitleP.'.index',compact('data'))
                    ->with('i', ($request->input('page', 1) - 1) * 5);	
    } 

     public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required',
            'path' => 'required',
            'price' => 'required|numeric',
            'popularity' => 'required|numeric',
        ]);

        $input = $request->all();
        if ($validator->passes()) {

            if($request->hasFile('path')){
                $input['path'] = ImageUpload::upload('/upload/supplements/',$request->file('path'));
            }

            $this->supplements->AddData($input);

            notificationMsg('success',$this->crudMessage('add',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required',
            'path' => 'required',
            'price' => 'required',
            'popularity' => 'required',
        ]);

        $input = $request->all();
        if ($validator->passes()) {

            if($request->hasFile('path')){
                $input['path'] = ImageUpload::upload('/upload/supplements/',$request->file('path'));
            }else{
                $input = array_except(['path'], $input);
            }
            $this->supplements->updateData($id,$input);

            notificationMsg('success',$this->crudMessage('update',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function destroy($id)
    {
        $this->supplements->destroyData($id);

        notificationMsg('success',$this->crudMessage('delete',$this->moduleTitleS));
        return redirect()->route($this->moduleTitleP.'.index');
    }

}

