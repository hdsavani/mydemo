<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Sportcategorie;
use Hash;
use Validator;

class SportsCatController extends AdminController
{
    public function __construct()
    {
		parent::__construct();
        $this->sportscat = new Sportcategorie;
        $this->moduleTitleS = 'sportcategories';
        $this->moduleTitleP = 'sportcategories';
        view()->share('moduleTitleP',$this->moduleTitleP);
        view()->share('moduleTitleS',$this->moduleTitleP);
    }   

    public function index(Request $request)
    {
        $data = $this->sportscat->getData($request->all());

        return view($this->moduleTitleP.'.index',compact('data'))
                    ->with('i', ($request->input('page', 1) - 1) * 15);
    } 

     public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        $input = $request->all();
        if ($validator->passes()) {
            $this->sportscat->AddData($input);

            notificationMsg('success',$this->crudMessage('add',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        $input = $request->all();
        if ($validator->passes()) {

            $this->sportscat->updateData($id,$input);

            notificationMsg('success',$this->crudMessage('update',$this->moduleTitleS));
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

     public function destroy($id)
    {
        $this->sportscat->destroyData($id);

        notificationMsg('success',$this->crudMessage('delete',$this->moduleTitleS));
        return redirect()->route($this->moduleTitleP.'.index');
    }

}
