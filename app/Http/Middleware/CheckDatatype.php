<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\AdminController as AdminController;

class CheckDatatype
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $error = new AdminController;

        $input = $request->all();

        if(!empty($input)){
            foreach ($input as $key => $value) {
                
                $intArrayColumns = ['agelimit_max','page','status','agelimit_min','age','showto'];
                $stringArrayColumns = ['token','fb_token','city','description','email','fullname'];
                $decimalArrayColumns = ['long','lon','lat'];
                $boolArrayColumns = ['gender'];
                $aArrayColumns = ['tags'];

                if (isset($intArrayColumns[$key])) {
                    if(!empty($value) && !is_numeric($value)){
                        $error = $error->getErrorMessage(460);
                        return response()->json($error);
                    }
                }elseif (isset($stringArrayColumns[$key])) {
                    if(!empty($value) && !is_string($value)){
                        $error = $error->getErrorMessage(460);
                        return response()->json($error);
                    }
                }elseif (isset($decimalArrayColumns[$key])) {
                    if(!empty($value) && !($this->is_decimal($value) || is_numeric($value))){
                        $error = $error->getErrorMessage(460);
                        return response()->json($error);
                    }
                }elseif (isset($boolArrayColumns[$key])) {
                    if(!empty($value) && !is_bool($value)){
                        $error = $error->getErrorMessage(460);
                        return response()->json($error);
                    }
                }elseif (isset($aArrayColumns[$key])) {
                    if(!empty($value) && !is_array($value)){
                        $error = $error->getErrorMessage(460);
                        return response()->json($error);
                    }
                }

            }
        }

        return $next($request);
    }

    public function is_decimal( $val )
    {
        return is_numeric( $val ) && floor( $val ) != $val;
    }

    public function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}
