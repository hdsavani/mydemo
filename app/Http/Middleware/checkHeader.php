<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Response;
use App\Http\Controllers\AdminController as AdminController;

class checkHeader
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    // protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    // public function __construct(Guard $auth)
    // {
    //     $this->auth = $auth;
    // }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $error = new AdminController;
        // if(!isset($_SERVER['HTTP_X_LASTINGDYNAMICS_CH'])){
        //     return Response::json($error->getErrorMessage(403));
        // }

        // if($_SERVER['HTTP_X_LASTINGDYNAMICS_CH'] != 'custom-gymhunt'){
        //     return Response::json($error->getErrorMessage(403));
        // }

        return $next($request);
    }
}