<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\AdminController as AdminController;
use App\Devices;

class createPretoken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $error = new AdminController;
        $devices = new Devices;

        if(!isset($_SERVER['HTTP_X_LASTINGDYNAMICS_PT'])){
            return response()->json($error->getErrorMessage(403));
        }

        $pt = $_SERVER['HTTP_X_LASTINGDYNAMICS_PT'];

        if(is_null($devices->checkToken($pt))){
            return response()->json($error->getErrorMessage(403));
        }

        return $next($request);
    }
}
