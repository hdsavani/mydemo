<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Config;
use DB;
use Input;
use Cache;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class Gym extends Authenticatable
{
	use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'price', 'address', 'latitude', 'longitude', 'price', 'opening_hours','tel_number','description','main_image'
    ];

    public function getData($input)
    {

    	$data = static::select("gyms.*"
                                ,DB::raw("(SELECT GROUP_CONCAT(tagsports.name SEPARATOR ', ') FROM tagsports
                                        INNER JOIN gyms_tagsports ON gyms_tagsports.tagsport_id = tagsports.id
                                        WHERE gyms_tagsports.gym_id = gyms.id
                                        GROUP BY gyms_tagsports.gym_id) as tags")
                                ,DB::raw("(SELECT GROUP_CONCAT(tagsports.id) FROM tagsports
                                        INNER JOIN gyms_tagsports ON gyms_tagsports.tagsport_id = tagsports.id
                                        WHERE gyms_tagsports.gym_id = gyms.id
                                        GROUP BY gyms_tagsports.gym_id) as tags_id")
                                ,DB::raw("(SELECT GROUP_CONCAT(tagsports.id) FROM tagsports
                                        INNER JOIN gyms_tagsports ON gyms_tagsports.tagsport_id = tagsports.id
                                        WHERE gyms_tagsports.gym_id = gyms.id AND gyms_tagsports.favorite = '1'
                                        GROUP BY gyms_tagsports.gym_id) as fav_tags_id"));

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where($column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where($column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->orderBy("gyms.id","DESC")->paginate(15);	
    }
     public function AddData($input)
    {
        return static::create(array_only($input,$this->fillable));
    }
    public function updateData($id, $input)
    {
        return static::find($id)->update(array_only($input,$this->fillable));
    }
    public function destroyData($id)
    {
        $find = static::find($id);

        ImageUpload::removeFile($find->main_image);

        return $find->delete();
    }
    public function totalGyms()
    {
        return static::count();
    }

    public function gyms($input)
    {
        $lat = $input['lat'];
        $lon = $input['lon'];

        $distance = "6371 * acos(cos(radians(" . $lat . ")) * cos(radians(gyms.latitude)) * cos(radians(gyms.longitude) - radians(" . $lon . ")) + sin(radians(" .$lat. ")) * sin(radians(gyms.latitude))) AS distance";
        $minutes = 129600;

        if(!Input::has('page') || Input::get('page') == 1){
            Cache::forget('gym-'.$input['user_id']);

            $data = Cache::remember('gym-'.$input['user_id'], $minutes, function () use($distance,$input) {
                    $data = static::select("gyms.id as gym_id",
                            "gyms.name as gym_name",
                            "gyms.address as address",
                            "gyms.opening_hours",
                            "gyms.main_image"
                            ,DB::raw("(SELECT GROUP_CONCAT(tagsports.name SEPARATOR ', ') FROM tagsports
                                    INNER JOIN gyms_tagsports ON gyms_tagsports.tagsport_id = tagsports.id
                                    WHERE gyms_tagsports.gym_id = gyms.id
                                    GROUP BY gyms_tagsports.gym_id) as tags")
                            ,DB::raw("(SELECT GROUP_CONCAT(tagsports.name SEPARATOR ', ') FROM tagsports
                                    INNER JOIN gyms_tagsports ON gyms_tagsports.tagsport_id = tagsports.id
                                    WHERE gyms_tagsports.gym_id = gyms.id AND gyms_tagsports.favorite = '1'
                                    GROUP BY gyms_tagsports.gym_id) as fav_tags")
                            ,DB::raw($distance))
                            ->leftjoin("gyms_tagsports as gyms_tag","gyms_tag.id","=","gyms.id");

                    if(!empty($input['price'])){
                        $data = $data->where("gyms.price","<=",$input['price']);
                    }

                    if(!empty($input['tags'])){
                        $data = $data->whereIn("gyms_tag.tagsport_id",$input['tags']);
                    }

                    return $data = $data->groupBy("gyms.id")
                                ->orderBy('distance')
                                ->get()->toArray();

            });
            $paginate = 10;
            $page = 1;

            $offSet = ($page * $paginate) - $paginate;  
            $itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);  
            $data = new Paginator($itemsForCurrentPage, count($data), $paginate, $page);
            return $data = $this->makeArray($data);
        }else{
            $data = Cache::get('gym-'.$input['user_id']);
            $paginate = 10;
            $page = Input::get('page');

            $offSet = ($page * $paginate) - $paginate;  
            $itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);  
            $data = new Paginator($itemsForCurrentPage, count($data), $paginate, $page);
            // $items = $result->items();
            // $result = $result->toArray();
            $data = $this->makeArray($data);
            $result = (array_values($data));
            return $result;
        }

    }

    private function makeArray($data)
    {
        $result = [];
        if($data->count()){
            foreach ($data as $key => $value) {
                $result[$key] = $value; 
                $result[$key]['tags'] = !empty($value->tags) ? explode(',', $value->tags) : [];
                $result[$key]['fav_tags'] = !empty($value->fav_tags) ? explode(',', $value->fav_tags) : [];
            }
        }

        return $result;
    }

    public function findGym($id)
    {
        return static::select("gyms.id",
            "gyms.name as gym_name",
            "gyms.address as address",
            "gyms.opening_hours",
            "gyms.main_image",
            "gyms.latitude",
            "gyms.longitude",
            "gyms.tel_number",
            "gyms.description"
            )->where('id',$id)->first();
    }
}
