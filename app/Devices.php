<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Devices extends Model
{
    public $table = "devices";
    public $timestamps = false;
    public $fillable = ['uuid','pre_token'];

    public function checkUUID($UUID)
    {
        return static::where('uuid',$UUID)->first();
    }

    public function updateDevice($input)
    {
        return static::where('uuid',$input['uuid'])->update(array_only($input,$this->fillable));
    }

    public function addDevice($input)
    {
        return static::create($input);
    }

    public function checkToken($token)
    {
        return static::where('pre_token',$token)->first();
    }
    
}
