<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Config;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'email', 'ban', 'ban_reason', 'gender', 'age', 'longitude', 'latitude','main_image','fb_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getData($input)
    {
        $data = static::select("users.*");

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where($column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where($column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->orderBy("users.id","DESC")->paginate(15);
    }

    public function AddData($input)
    {
        return static::create(array_only($input,$this->fillable));
    }

    public function destroyData($id)
    {
        return static::find($id)->delete();
    }

    public function updateData($id, $input)
    {
        return static::find($id)->update(array_only($input,$this->fillable));
    }

    public function userBan($input)
    {
        return static::where('id',$input['id'])->update(array('ban' => 1, 'ban_reason' => $input['ban_reason']));
    }

    public function userRevoke($id)
    {
        return static::where('id',$id)->update(array('ban' => 0, 'ban_reason' => ''));
    }

    public function getUserLists()
    {
        return static::pluck('fullname','id')->all();
    }
    public function totalUsers()
    {
        return static::count();
    }

    public function checkEmail($email)
    {
        return static::where('email',$email)->first();
    }

    public function findUser($input)
    {
        return static::where('fb_id',$input['id'])->first();
    }

    public function createNewUser($input)
    {
        return static::create($input);
    }
}
