<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Config;

class Workouts extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_musclesgroup','name','path1','path2','difficulty','description','repetition','series',
    ];

    public function getData($input)
    {
    	$data = static::select("workouts.*","musclesgroups.name as groupname")
                        ->join("musclesgroups","musclesgroups.id","=","workouts.id_musclesgroup");

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where("workouts.".$column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where("workouts.".$column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->orderBy("workouts.id","DESC")->paginate(10);	
    }
     public function AddData($input)
    {
        return static::create(array_only($input,$this->fillable));
    }
    public function updateData($id, $input)
    {
        return static::find($id)->update(array_only($input,$this->fillable));
    }
    public function destroyData($id)
    {
        $find = static::find($id);

        ImageUpload::removeFile($find->path1);
        ImageUpload::removeFile($find->path2);

        return $find->delete();
    }

    public function totalWorkouts()
        {
            return static::count();
        }    

    public function exceriseId($id)
    {
        return static::select('name','path1 as image1', 'path2 as image2','difficulty', 'description', 'repetition', 'series')->where('id',$id)->first();
    }
}
