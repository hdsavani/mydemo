<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Error extends Model
{
    public $timestamps = false;
    protected $fillable = ['description','code','language'];
    protected $table = 'errors';

    public function getLanguageList()
    {
        return DB::table('languages')->pluck("language","id");
    }

    public function addLanguage($input)
    {
        return static::create($input);
    }

    public function checkLanguage($input)
    {
        return static::where('language',$input['language'])->first();
    }

    public function getLanguages()
    {
        return static::get();
    }

    public function removeLanguage($id)
    {
        return static::where("id",$id)->delete();
    }

    public function editLanguage($input)
    {
        return static::where("id",$input['id'])->update(['language'=>$input['language']]);
    }

    public function getContents($input)
    {
        $lang = $input['language'];
        return DB::table('errors')
            ->select("errors.code"
                ,DB::raw("(CASE WHEN appcontents.value IS NOT NULL
                            THEN appcontents.value
                            ELSE default_contents.name END
                            ) as value"))
            ->leftjoin("language",function($join) use ($lang){
                $join->on("language.code","=","errors.error_code")
                    ->on("errors.language","=",DB::raw("$lang"));
            })
            ->where('errors.language',0)
            ->get();
    }

    public function getAppcontent($input)
    {
        return static::where('language',$input['id_language'])
            ->groupBy("code")
            ->get();
    }

    public function defaultAppcontent()
    {
        return static::all();
    }

    public function updateContent($input)
    {
        if(empty($input['id_language'])){
            $input['id_language'] = NULL;
        }

        if(isset($input['id'])){
            $find = static::where('code',$input['id'])
                ->where('language',$input['id_language'])
                ->update(array('description'=>$input['value']));
            return static::where('code',$input['id'])
                ->where('language',$input['id_language'])->first();
        }
        $insert['code'] = $input['id_content'];
        $insert['description'] = $input['value'];
        $insert['language'] = $input['id_language'];
        return static::create(array_only($insert,$this->fillable));
    }
}