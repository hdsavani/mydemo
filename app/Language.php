<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Config;

class Language extends Authenticatable
{
    use Notifiable;

   public $timestamps = false;
    protected $fillable = ['language'];



    public function getData($input)
    {
    	$data = static::select("languages.*");

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where($column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where($column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->orderBy("languages.id","DESC")->paginate(15);
    }

    public function AddData($input)
    {
        return static::create(array_only($input,$this->fillable));
    }

    public function destroyData($id)
    {
        return static::find($id)->delete();
    }

    public function updateData($id, $input)
    {
        return static::find($id)->update(array_only($input,$this->fillable));
    }
    public function getLanList()
    {
        return static::pluck('language','id')->all();
    }

}
