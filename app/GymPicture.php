<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Config;

class GymPicture extends Authenticatable
{
	use Notifiable;

    protected $table = 'gym_pictures';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['gym_id', 'path'];

    public function getData($id)
    {
    	return static::select("gym_pictures.*")->where('gym_id',$id)->paginate(10);	
    }

    public function AddData($input)
    {
        $input['created_at'] = date('Y-m-d H:i:s');
        return static::create($input);
    }

    public function destroyData($id)
    {
        $find = static::find($id);

        ImageUpload::removeFile($find->path);

        return $find->delete();
    }
}
